# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Nov 10, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

class Package(object):
    """
    Implementation of a package dependency tree
    """
    
    def __init__(self, name):
        """
        Constructor
        """
        self.__name = name
        self.__packageLocation = ""
        self.__isInstalled = False
        self.__dependencies = []
        
    def GetName(self):
        """
        Get the name of the package
        """
        return self.__name
    
    def SetName(self, name):
        """
        Set the name of the package
        """
        self.__name = name
        
    def SetLocation(self, loc):
        """
        Set relative source location to the input cvmfs
        """
        self.__location = loc
        
    def GetLocation(self):
        """
        Get relative source location to the input cvmfs
        """
        return self.__location
    
    def IsInstalled(self):
        """
        Check whether package is installed
        """
        return self.__isInstalled
    
    def SetIsInstalled(self, isInstalled):
        """
        Mark package as installed
        """
        self.__isInstalled = isInstalled
        
    def AddDependency(self, package):
        """
        Add package this package depends on to the dependency list
        """
        self.__dependencies.append(package)
        
    def GetListOfDependencies(self):
        """
        Get a list of packages this package depends on
        """
        return self.__dependencies
    
    def IsDependentOn(self, packagename):
        """
        Check (recursively) if the package is dependent on the package asked. In this case also parents are checked for
        dependency
        :"""
        result = False
        for package in self.__dependencies:
            if package.GetName() == packagename:
                result = True
                break
            else:
                if package.IsDependentOn(packagename):
                    result = True
                    break
        return result