# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Handling alice packages from cvmfs and dependencies in order to use them on
Cray Linux systems

:since: Oct 16, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
"""

import sqlite3, os, getpass, sys
from getopt import getopt, GetoptError

class Package(object):
    """
    Implementation of a package dependency tree
    """
    
    def __init__(self, name):
        """
        Constructor
        """
        self.__name = name
        self.__packageLocation = ""
        self.__isInstalled = False
        self.__dependencies = []
        
    def GetName(self):
        """
        Get the name of the package
        """
        return self.__name
    
    def SetName(self, name):
        """
        Set the name of the package
        """
        self.__name = name
        
    def SetLocation(self, loc):
        """
        Set relative source location to the input cvmfs
        """
        self.__location = loc
        
    def GetLocation(self):
        """
        Get relative source location to the input cvmfs
        """
        return self.__location
    
    def IsInstalled(self):
        """
        Check whether package is installed
        """
        return self.__isInstalled
    
    def SetIsInstalled(self, isInstalled):
        """
        Mark package as installed
        """
        self.__isInstalled = isInstalled
        
    def AddDependency(self, package):
        """
        Add package this package depends on to the dependency list
        """
        self.__dependencies.append(package)
        
    def GetListOfDependencies(self):
        """
        Get a list of packages this package depends on
        """
        return self.__dependencies
    
    def IsDependentOn(self, packagename):
        """
        Check (recursively) if the package is dependent on the package asked. In this case also parents are checked for
        dependency
        :"""
        result = False
        for package in self.__dependencies:
            if package.GetName() == packagename:
                result = True
                break
            else:
                if package.IsDependentOn(packagename):
                    result = True
                    break
        return result
    
class PackageDB(object):
    """
    Database containing packages and dependencies
    """ 

    def __init__(self, dbname):
        """
        Constructor, opens a connection to the database and creates the tables if new
        """
        self.__isNew = False if os.path.exists(dbname) else True
        self.__database = sqlite3.connect(dbname)
        if self.__isNew:
            self.__CreateTables()
            
    def __del__(self):
        """
        Destructor closing connection to the database
        """
        self.__database.close()
    
    def __CreateTables(self):
        """
        Create the two tables (list of packages - dependencies) in the database
        """
        cursor = self.__database.cursor()
        cursor.execute("CREATE TABLE PACKAGES(name VARCHAR(50), location VARCHAR(100), isInstalled INTEGER)")
        cursor.execute("CREATE TABLE DEPENDENCIES(package VARCHAR(50), dependency VARCHAR(50))")
        cursor.execute("CREATE TABLE INSTALLATION(package VARCHAR(50), requester VARCHAR(50), status VARCHAR(5))")
        self.__database.commit()
        
    def InsertPackage(self, package):
        """
        Insert a new package into the database including direct dependencies
        """
        cursor = self.__database.cursor()
        cursor.execute("INSERT INTO PACKAGES VALUES (?,?,?)", (package.GetName(), package.GetLocation(), 1 if package.IsInstalled() else 0))
        for dep in package.GetListOfDependencies():
            cursor.execute("INSERT INTO DEPENDENCIES VALUES (?, ?)", (package.GetName(), dep.GetName()))
        self.__database.commit()
    
    def FindPackage(self, packagename):
        """
        Find a package in the database and all the dependencies recursively
        """
        cursor = self.__database.cursor()
        cursor.execute("SELECT * FROM PACKAGES WHERE name = \'%s\'" %(packagename))
        entries = cursor.fetchall()             # package is unique
        if not len(entries):
            print "Package %s not found" %(packagename)
            return None
        entry = entries[0]
        result = Package(entry[0])
        result.SetLocation(entry[1])
        result.SetIsInstalled(bool(entry[2]))
        cursor.execute("SELECT dependency FROM DEPENDENCIES WHERE package = \'%s\'" %(packagename))
        dependencies = cursor.fetchall()
        for dep in dependencies:
            result.AddDependency(self.FindPackage(dep[0]))
        return result
    
    def GetListOfPackages(self, isInstalled):
        """
        Query list of packages (available or installed) from the package database
        """
        resultlist = []
        querycommand = ""
        if isInstalled:
            querycommand = "SELECT name FROM PACKAGES WHERE isInstalled = 1"
        else:
            querycommand = "SELECT name FROM PACKAGES"
        cursor = self.__database.cursor()
        cursor.execute(querycommand)
        res = cursor.fetchall()
        for entry in res:
            resultlist.append(str(entry[0]))
        return resultlist
    
    def SetInstalled(self, packagename):
        """
        Mark package as installed
        """
        self.__UpdateInstallationValue(packagename, 1)
    
    def SetUninstalled(self, packagename):
        """
        Mark package as uninstalled
        """
        self.__UpdateInstallationValue(packagename, 0)
    
    def __UpdateInstallationValue(self, packagename, value):
        """
        Change installation status of a package (if it is staged in the staging location
        or removed from the staging location
        """
        cursor = self.__database.cursor()
        # First check if package exists in the database
        cursor.execute("SELECT * FROM PACKAGES WHERE name = \'%s\'" %(packagename))
        queryresult = cursor.fetchall()
        if not len(queryresult):
            print "Package %s not listed" %(packagename)
            return
        cursor.execute("UPDATE PACKAGES SET isInstalled = %d WHERE name = \'%s\'" %(value, packagename))
        self.__database.commit()
        
    def InsertInstallationStatus(self, package, requester, status = "RUN"):
        """
        Insert a new installation job
        """
        cursor = self.__database.cursor()
        cursor.execute("INSERT INTO INSTALLATION VALUES(?,?,?)", (package, requester, status))
        self.__database.commit()
    
    def UpdateInsallationStatus(self, package, status):
        """
        Update the installation status
        
        Status codes:
        PEND - pending processes
        RUN  - running processes
        COMP - completed processes
        ERR  - failed processes
        """
        cursor = self.__database.cursor()
        cursor.execute("SELECT * FROM INSTALLATION WHERE package = \'%s\'" %(package))
        entries = cursor.fetchall()
        if not len(entries):
            print "No installation job for package %s registered" %(package)
            return 
        cursor.execute("UPDATE INSTALLATION SET status = \'%s\' where package = \'%s\'" %(status, package))
        self.__database.commit()
        if status == "COMP":
            self.SetInstalled(package)
    
    def CheckInstallationStatus(self, packagename):
        """
        Check the status of an installation process
        """
        cursor = self.__database.cursor()
        cursor.execute("SELECT status FROM INSTALLATION WHERE package = \'%s\'" %(packagename))
        entry = cursor.fetchone()
        if not entry:
            return "NONE"
        else:
            return entry[0]
        
    def GetInstallationRequests(self):
        """
        Get List of packages 
        """
        return self.__GetInstListForStatus("PEND")
        
    def GetRunningInstallations(self):
        """
        Get List of installations which are currently running
        """
        return self.__GetInstListForStatus("RUN")

    def __GetInstListForStatus(self, status):
        """
        Get all entries in the installation table for a given status
        """
        cursor = self.__database.cursor()
        cursor.execute("SELECT package from INSTALLATION WHERE status = \'%s\'" %(status))
        enlist = cursor.fetchall()
        result = []
        for entry in enlist:
            result.append(str(entry[0]))
    
class PackageManager(object):
    """
    Implementation of a package management system which handles packages on cvmfs and
    on hopper
    """
    
    def __init__(self):
        """
        Constructor
        """
        self.__sourcelocation = "/cvmfs/alice.cern.ch/"
        self.__hopperlocation = "/project/projectdirs/alice/hpc/cvmfs/alice.cern.ch"
        self.__environment = "x86_64-2.6-gnu-4.1.2"
        self.__packagedb = PackageDB("/project/projectdirs/alice/hpc/packages.db")
        
    def GetSourcePackages(self):
        return self.__packagedb.GetListOfPackages(False)
    
    def GetInstalledPackages(self):
        return self.__packagedb.GetListOfPackages(True)
        
    def UpdateDatabase(self):
        """
        Update package database
        """
        sourcepackages = self.FindSourcePackages()
        for category, packagelist in sourcepackages.iteritems():
            for package in packagelist:
                modulename = "%s/%s" %(category, package)
                if not self.__packagedb.FindPackage(modulename):
                    self.InsertNewSource(modulename)
        
    def FindSourcePackages(self):
        """
        Find all packages installed (via the modulesfiles)
        """
        modulebase = self.__GetSourceModuleBase()
        categories = os.listdir(modulebase)
        result = {}
        for category in categories:
            result[category] = os.listdir("%s/%s" %(modulebase, category))
        return result
    
    def ParseModulefile(self, modulefile):
        result = {"dependencies":[]}
        reader = open(modulefile, "r")
        for line in reader:
            if "#" in line:
                continue
            if "module load" in line:
                line = line.replace("\n","").replace("module load", "")
                mods = line.split(" ")
                for mod in mods:
                    mod = mod.replace(" ","")
                    if len(mod):
                        result["dependencies"].append(mod)
        return result
    
    def InsertNewSource(self, modulename):
        """
        Insert package into pacakge database
        """
        packageinfos = self.ParseModulefile("%s/%s" %(self.__GetSourceModuleBase(), modulename))
        package = Package(modulename)
        package.SetLocation("Packages/%s"%(modulename))
        for dep in packageinfos["dependencies"]:
            package.AddDependency(Package(dep))
        self.__packagedb.InsertPackage(package)
    
    def InstallPackage(self, packagename):
        package = self.__packagedb.FindPackage(packagename)
        if not package:
            print "Error: package %s not registered in the package database"
        if package.IsInstalled():
            print "Package %s already installed" %(packagename)
            return
        self.__packagedb.InsertInstallationStatus(packagename, getpass.getuser(), "PEND")
        for dep in package.GetListOfDependencies():
            if not dep.IsInstalled():
                self.InstallPackage(dep.GetName())
        # Install package
        if not os.path.exists(self.__sourcelocation):
            print "Machine does not have cvmfs-access"
            return
        print "Installing package %s on hopper" %(packagename)
        self.__packagedb.UpdateInsallationStatus(packagename, "RUN")
        # 1st package, then modulefile
        if not "BASE" in packagename:
            packageout = "%s/%s/Packages/%s" %(self.__hopperlocation, self.__environment, packagename.split("/")[0])
            if not os.path.exists(packageout):
                os.makedirs(packageout, 0775)
            sourceloc = "%s/%s/%s" %(self.__sourcelocation, self.__environment, package.GetLocation())
            targetloc = "%s/%s/%s" %(self.__hopperlocation, self.__environment, package.GetLocation())
            os.system("rsync -avd \"%s/\" \"%s/\"" %(sourceloc, targetloc))
        moduleout = "%s/%s" %(self.__GetInstallModuleBase(), packagename.split("/")[0])
        if not os.path.exists(moduleout):
            os.makedirs(moduleout)
        inputmodule = "%s/%s" %(self.__GetSourceModuleBase(), packagename)
        os.system("cp %s %s/" %(inputmodule, moduleout))
        self.__packagedb.UpdateInsallationStatus(packagename, "COMP")
    
    def IsInstalled(self, packagename):
        """
        Check whether a package is installed
        """
        package = self.__packagedb.FindPackage(packagename)
        if not package:
            return False
        return package.IsInstalled() 
    
    def __GetSourceModuleBase(self):
        return "%s/%s/Modules/modulefiles" %(self.__sourcelocation, self.__environment)
    
    def __GetInstallModuleBase(self):
        return "%s/%s/Modules/modulefiles" %(self.__hopperlocation, self.__environment)
    
class PackageDaemon(object):
    """
    Daemon process running on a pdsf transfer node and checking for after a certain time interval whether there are new installation requests
    """
    
    def __init__(self):
        """
        Constructor
        """
        self.__packman = PackageManager()
        self.__timeInterval = 1
        
    def SetTimeInterval(self, timeint):
        """
        Set update time cycle
        """
        self.__timeInterval = timeint
        
    def Run(self):
        """
        @TODO: Implement daemon tasks
        """
        pass
    
def CreatePackageDB():
    """
    Build or update package db
    """
    packman = PackageManager()
    packman.UpdateDatabase()
    
def ListPackages(option):
    """
    List packages
    """
    packman = PackageManager()
    packages = None
    if option == "avail":
        print "Available packages:"
        print "=================================="
        packages = packman.GetSourcePackages()
    elif option == "installed":
        print "Installed packages:"
        print "=================================="
        packages = packman.GetInstalledPackages()
    if packages:
        for package in packages:
            print package
    else:
        print "No installed packages"
            
def InstallPackage(packagename):
    """
    Run installation of a new package
    """
    packman = PackageManager()
    packman.InstallPackage(packagename)
    
def RunDaemon(updateint = 0):
    """
    Run installation daemon that checks every time interval for new installation requests and installs the software 
    """
    daemon = PackageDaemon()
    if updateint > 0:
        daemon.SetTimeInterval(updateint)
    daemon.Run()

def main():
    """
    Main function, steering package tasks
    """
    try:
        opts, args = getopt(sys.argv[1:], "cdi:l:t:")
        doDaemon = False
        timeint = 0
        for o,a in opts:
            if o == "-c":
                CreatePackageDB()
            elif o == "-l":
                ListPackages(str(a))
            elif o == "-i":
                InstallPackage(str(a))
            elif o == "-d":
                doDaemon = True
            elif o == "-t":
                timeint = int(a)
        if doDaemon:
            RunDaemon(timeint)
    except GetoptError as e:
        print "Error: %s" %(str(e))
        sys.exit(1)

if __name__ == "__main__":
    main()
